package services;

import java.util.Scanner;

public class UserInteraction {

    // class responsible for dealing with the file manipulation.
    FileManipulation fm = new FileManipulation();

    // Entry point method for starting the user interaction loop
    public void startUserInteraction(){
        System.out.println("\n\nWelcome to the file system manager!");
        userInteractionLoop();
        System.out.println("\n\tTake care!\n");
    }

    // recursive method for showing user choises.
    private void userInteractionLoop(){

        System.out.println();
        System.out.println("1. Get all files in resources.");
        System.out.println("2. Get all files by extension.");
        System.out.println("3. Get information from txt.");
        System.out.println("4. Exit.");
        System.out.print("Enter your needs: ");

        // getUserInput returns a string from the user via the terminal using Scanner.
        String userInput = getUserInput();

        switch(userInput){
            case "1":
                fm.printAllFilesInDir();
                userInteractionLoop();
                break;
            case "2":
                chooseFileExtension();
                userInteractionLoop();
                break;
            case "3":
                chooseFileInfo();
                userInteractionLoop();
                break;
            case "4":
                return;
            default:
                System.out.println("\n\tInvalid input!");
                userInteractionLoop();
                break;
        }
    }


    // Method for getting and returning user input.
    private String getUserInput(){

        try {
            Scanner scan = new Scanner(System.in);
            String input = scan.nextLine();
            return input.trim();
        }
        catch (Exception e){
            System.out.println(e);
            return null;
        }
    }


    // recursive method for showing user choises.
    private void chooseFileInfo(){

        System.out.println();
        System.out.println("1. Name of the file.");
        System.out.println("2. File size.");
        System.out.println("3. Number of lines in file.");
        System.out.println("4. Check if word exists in file.");
        System.out.println("5. Check word count in file.");
        System.out.println("6. Return to main menu.");
        System.out.print("So what do you need?: ");

        String userInput = getUserInput();

        switch(userInput){
            case "1":
                fm.printFileName();
                chooseFileInfo();
                break;
            case "2":
                fm.printFileSize();
                chooseFileInfo();
                break;
            case "3":
                fm.printFileLineNumber();
                chooseFileInfo();
                break;
            case "4":
                fm.checkIfWordExists(chooseWord());
                chooseFileInfo();
                break;
            case "5":
                fm.checkWordOccurrences(chooseWord());
                chooseFileInfo();
                break;
            case "6":
                return;
            default:
                System.out.println("\n\tInvalid input!");
                chooseFileInfo();
                break;
        }
    }


    private String chooseWord(){
        System.out.print("Choose a word: ");
        return getUserInput();
    }


    private void chooseFileExtension(){
        System.out.print("Choose file extension: ");
        String extension = getUserInput();
        fm.printAllFilesByExt(extension);
    }


}
