package services;

import java.io.File;
import java.util.ArrayList;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import logging.*;


class FileManipulation {

    Logger logger = new Logger();
    File txtFile = null;

    // first occurrence of a .txt file will be set as the txtFile.
    public FileManipulation(){
        txtFile = getTxtFile();
    }


    // method to init the .txt file in the resources folder.
    private File getTxtFile(){

        File[] fileList = new File("../src/resources").listFiles();
        for (File file : fileList){
            if (file.isFile()){
                String fileName = file.getName();
                if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0){
                    String fileNameExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
                    if (fileNameExtension.equals("txt")){
                        return file;
                    }
                }
            }
        }
        return null;
    }


    public void printAllFilesInDir(){

        try {
            logger.startTime();
            // fileList is an array of File-objects representing
            // all files in the "resources" folder.
            File[] fileList = new File("../src/resources").listFiles();

            // print names of all files with the getName() method.
            System.out.println();
            for (File file : fileList){
                if (file.isFile()){
                    System.out.println("\t" + file.getName());
                }
            }
            logger.stopTime();
            logger.logText("printed all files in directory.");
        }
        // If the directory does not exist / gets deleted etc.
        catch(NullPointerException e){
            System.out.println(e);
        }
    }


    public void printAllFilesByExt(String extension){

        try {
            logger.startTime();
            File[] fileList = new File("../src/resources").listFiles();

            ArrayList<String> fileNames = new ArrayList<String>();

            // Find all files in fileList with matching file extension and
            // add them to fileNames.
            for (File file : fileList){
                if (file.isFile()){
                    String fileName = file.getName();
                    if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0){

                        // get the extension from the file name and compare with argument
                        // add too fileNames if a match.
                        String fileNameExtension = fileName.substring(fileName.lastIndexOf(".") + 1);
                        if (fileNameExtension.equals(extension)){
                            fileNames.add(fileName);
                        }
                    }
                }
            }

            // if fileNames contains any files: print all files
            if (fileNames.size() > 0){
                System.out.println();
                for (String fileName : fileNames){
                    System.out.println("\t" + fileName);
                }
            }
            // Notify user if no files found with mathcing extension
            else {
                System.out.println("No files with this extension!");
            }

            logger.stopTime();
            logger.logText("Printed all filenames with extension " + extension + ".");
        }
        catch(NullPointerException e){
            System.out.println(e);
        }
    }


    public void printFileName(){
        try {
            logger.startTime();
            System.out.println("\n\t" + txtFile.getName());
            logger.stopTime();
            logger.logText("Printed .txt file name: " + txtFile.getName() + ".");
        }
        catch(NullPointerException e){
            System.out.println(e);
        }
    }


    public void printFileSize(){

        try {
            // fastest way.
            logger.startTime();
            System.out.println("\n\t" +txtFile.getName() + " is " + txtFile.length() / 1024 + " kb");
            logger.stopTime();
            logger.logText("Printed file size: " + txtFile.length() / 1024 + "kb.");

            // Alternative way.
            /*
            FileInputStream stream = new FileInputStream(txtFile.getAbsolutePath());

            int data = stream.read();
            int numberOfBytes = 0;

            // read() returns -1 when there is no more data
            while (data != -1){
                numberOfBytes++;
                data = stream.read();
            }

            System.out.println("\n" +txtFile.getName() + " is " + numberOfBytes / 1024 + " kb.");
            */
        }
        /*
        catch(IOException e){
            System.out.println(e);
        }
        */
        catch(Exception e){
            System.out.println(e);
        }
    }


    // Method for printing number of lines in the txt-file.
    public void printFileLineNumber(){
        try {
            logger.startTime();
            BufferedReader buffreader = new BufferedReader(new FileReader(txtFile.getAbsolutePath()));

            int numberOfLines = 0;
            String line;

            // count line by line
            while ((line = buffreader.readLine()) != null){
                numberOfLines++;
            }

            System.out.println("\n\tNumber of lines: " + numberOfLines);
            logger.stopTime();
            logger.logText("Printed number of lines: " + numberOfLines + ".");
        }
        catch(IOException e){
            System.out.println(e);
        }
        catch(Exception e){
            System.out.println(e);
        }
    }


    // Method for checking if a word exists in the txt-file
    public void checkIfWordExists(String word){

        Scanner scan = null;

        try {
            logger.startTime();
            scan = new Scanner(txtFile);
            String wordLowerCase = word.toLowerCase();

            // regex to get whole separated words.
            String regex = "\\b"+wordLowerCase+"\\b";
            Pattern pattern = Pattern.compile(regex);

            // read line by line with the Scanner object
            while (scan.hasNextLine()){

                String line = scan.nextLine().toLowerCase();
                Matcher matcher = pattern.matcher(line);

                //check if any matches.
                while (matcher.find()){
                    System.out.println("\n\t" + word + " exists in file!");
                    logger.stopTime();
                    logger.logText(word + " exists in the file.");
                    return;
                }
            }

            System.out.println("\n\t" + word + " does not exist in file!");
            logger.stopTime();
            logger.logText(word + " does not exist in the file.");
        }
        catch(Exception e){
            System.out.print(e);
        }
        finally {
            scan.close();
        }
    }

    // Method for counting specific word occurences in the txt-file.
    // Same as the method above, except this one adds up all found occurences
    // and logs the total number.
    public void checkWordOccurrences(String word){  

        Scanner scan = null;
        
        try {
            logger.startTime();
            scan = new Scanner(txtFile);
            String wordLowerCase = word.toLowerCase();
            int number = 0;

            String regex = "\\b"+wordLowerCase+"\\b";
            Pattern pattern = Pattern.compile(regex);

            while (scan.hasNextLine()){

                String line = scan.nextLine().toLowerCase();
                Matcher matcher = pattern.matcher(line);

                // while there are matched in the line.
                while (matcher.find()){
                    number++;
                }
            }

            System.out.println("\n\t" + word + " occurs " + number + " times in " + txtFile.getName());
            logger.stopTime();
            logger.logText(word + " occurs " + number + " times.");
        }
        catch(Exception e){
            System.out.print(e);
        }
        finally {
            scan.close();
        }
    }
}
