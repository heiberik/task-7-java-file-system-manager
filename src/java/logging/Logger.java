package logging;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.IOException;
import java.io.FileWriter;

// class responsible for logging.
public class Logger {

    long time = 0;
    int execTime = 0;

    // starts the timer
    public void startTime(){
        time = System.nanoTime();
    }   

    // stops the timer. Converts the duration to milli seconds. 
    public void stopTime(){
        long endTime = System.nanoTime();
        long duration = (endTime - time) / 1000000;
        execTime = Math.toIntExact(duration);
    }

    // Method to format and return current data as a String.
    private String getDate(){
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-dd-MM 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

    // method for logging text in logs/logs.txt. 
    // The method will create the file if it does not exist 
    // and will append to it, not delete it. 
    public void logText(String text){
        try {
            FileWriter myWriter = new FileWriter("../src/logs/logs.txt", true);
            myWriter.write(getDate() + ":\n"+ text + "\nThe function took " +execTime+ "ms to execute.\n\n");
            myWriter.close();
        }
        catch(IOException e){
            System.out.println(e);
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
}
