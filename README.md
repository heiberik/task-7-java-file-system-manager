# Task 2 - Java - File system manager

## Overview
Welcome to the file system manager. To run the program you can navigate to the /out folder and enter "java -jar Program.jar". Proceed by following the instructions given in the terminal and enter your prefered actions by using number 1-6. 

Performance of the algorithms is logged in logs.txt.


## Screenshots

![Screenshot 1](/src/screenshots/screen1.png?raw=true "Screen 1")
![Screenshot 2](/src/screenshots/screen2.png?raw=true "Screen 2")
![Screenshot 3](/src/screenshots/screen3.png?raw=true "Screen 3")
